public class Vermenigvuldiger implements IBereken {
    @Override
    public double bereken(double a, double b) {
        return a*b;
    }
}

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class RekenmachineGUI extends Application {

    public void start(Stage stage) {
        stage.setTitle("Rekenmachine opdracht");

        // Operator A box
        HBox hBoxA = new HBox();
        hBoxA.setSpacing(10);
        hBoxA.setPadding(new Insets(0, 20, 10, 20));
        Label lbA = new Label("Input A: ");
        TextField txInpA = new TextField("");
        hBoxA.getChildren().addAll(lbA, txInpA);

        // Operator B box
        HBox hBoxB = new HBox();
        hBoxB.setSpacing(10);
        hBoxB.setPadding(new Insets(0, 20, 10, 20));
        Label lbB = new Label("Input B: ");
        TextField txInpB = new TextField("");
        hBoxB.getChildren().addAll(lbB, txInpB);

        // Answer box
        HBox hBoxAnswer = new HBox();
        hBoxAnswer.setSpacing(10);
        hBoxAnswer.setPadding(new Insets(0, 20, 10, 20));
        Label lbAnswer = new Label("Answer: ");
        TextField txInpAnswer = new TextField("");
        hBoxAnswer.getChildren().addAll(lbAnswer, txInpAnswer);

        VBox vBox = new VBox();
        vBox.getChildren().add(hBoxA);
        vBox.getChildren().add(hBoxB);
        vBox.getChildren().add(hBoxAnswer);

        // All buttons
        Button btnClear = new Button("Clear");
        btnClear.setMaxWidth(Double.MAX_VALUE);
        btnClear.setOnAction( (event) -> {
            txInpA.setText("0.0");
            txInpA.setText("0.0");
        });

        Button btnAdd = new Button("Add");
        btnAdd.setMaxWidth(Double.MAX_VALUE);
        btnAdd.setOnAction( (event) -> {
            Double answer = new Rekenmachine(OperatorEnum.OPTELLEN)
                    .bereken( Double.parseDouble(txInpA.getText()),Double.parseDouble(txInpB.getText()));
            txInpAnswer.setText( answer.toString() );
        });

        Button btnSubtract = new Button("Subtract");
        btnSubtract.setMaxWidth(Double.MAX_VALUE);
        btnSubtract.setOnAction( (event) -> {
            Double answer = new Rekenmachine(OperatorEnum.AFTELLEN)
                    .bereken( Double.parseDouble(txInpA.getText()),Double.parseDouble(txInpB.getText()));
            txInpAnswer.setText( answer.toString() );
        });

        Button btnMultiply = new Button("Multiply");
        btnMultiply.setMaxWidth(Double.MAX_VALUE);
        btnMultiply.setOnAction( (event) -> {
            Double answer = new Rekenmachine(OperatorEnum.VERMENIGVULDIGEN)
                    .bereken( Double.parseDouble(txInpA.getText()),Double.parseDouble(txInpB.getText()));
            txInpAnswer.setText( answer.toString() );
        });

        Button btnDivide = new Button("Divide");
        btnDivide.setMaxWidth(Double.MAX_VALUE);
        btnDivide.setOnAction( (event) -> {
            Double answer = new Rekenmachine(OperatorEnum.DELEN)
                    .bereken( Double.parseDouble(txInpA.getText()),Double.parseDouble(txInpB.getText()));
            txInpAnswer.setText( answer.toString() );
        });

        HBox hbButtons = new HBox();
        hbButtons.setSpacing(10);
        hbButtons.setPadding(new Insets(0, 20, 10, 20));
        hbButtons.getChildren().addAll(btnClear, btnAdd, btnSubtract, btnDivide, btnMultiply);

        FlowPane flowPane = new FlowPane();
        flowPane.setHgap(8);
        flowPane.setVgap(8);
        flowPane.getChildren().add(vBox);
        flowPane.getChildren().add(hbButtons);

        Scene scene = new Scene(flowPane);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(RekenmachineGUI.class);
    }
}

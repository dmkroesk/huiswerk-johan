
public class Rekenmachine {

    private IBereken berekening;

    public Rekenmachine(OperatorEnum operator) {
        try {
            this.berekening = GetBerekening(operator);
        } catch(Exception e) {
            System.out.println(e.toString());
        }
    }

    private IBereken GetBerekening(OperatorEnum operator) throws Exception {
        switch (operator) {
            case OPTELLEN:
                return new Opteller();
            case AFTELLEN:
                return new Afteller();
            case VERMENIGVULDIGEN:
                return new Vermenigvuldiger();
            case DELEN:
                return new Deler();
            default:
                throw new Exception("Onbekende operator");
        }
    }

    public double bereken(double a, double b) {
        return this.berekening.bereken(a,b);
    }
}
